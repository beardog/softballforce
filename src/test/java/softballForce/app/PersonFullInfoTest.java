package softballForce.app;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
//


public class PersonFullInfoTest {

  @Test
  public void readFullNameBasic() {
    String sn = "БОНДАРЕНКО";
    String fn = "Олександр";
    String mn = "Миколайович";

    String name = sn + " " + fn + " " + mn;

    PersonFullInfo personFullInfo = new PersonFullInfo();
    boolean readResult = personFullInfo.readFullName(name);

    assertEquals(readResult, true);
    assertEquals(sn, personFullInfo.surname);
    assertEquals(fn, personFullInfo.firstName);
    assertEquals(mn, personFullInfo.middleName);
  }

  @Test
  public void readFullNameSnApos() {
    String sn = "НЕТРЕБ'ЯК";
    String fn = "Юрій";
    String mn = "Іванович";


    String name = sn + " " + fn + " " + mn;

    PersonFullInfo personFullInfo = new PersonFullInfo();
    boolean readResult = personFullInfo.readFullName(name);

    assertEquals(readResult, true);
    assertEquals(sn, personFullInfo.surname);
    assertEquals(fn, personFullInfo.firstName);
    assertEquals(mn, personFullInfo.middleName);
  }

  @Test
  public void readFullNameFnApos() {
    String sn = "БОНДАРЕНКО";
    String fn = "В'ячеслав";
    String mn = "Миколайович";

    String name = sn + " " + fn + " " + mn;

    PersonFullInfo personFullInfo = new PersonFullInfo();
    boolean readResult = personFullInfo.readFullName(name);

    assertEquals(readResult, true);
    assertEquals(sn, personFullInfo.surname);
    assertEquals(fn, personFullInfo.firstName);
    assertEquals(mn, personFullInfo.middleName);
  }

  @Test
  public void readFullNameMnApos() {
    String sn = "БОНДАРЕНКО";
    String fn = "Олександр";
    String mn = "В'ячеславович";

    String name = sn + " " + fn + " " + mn;

    PersonFullInfo personFullInfo = new PersonFullInfo();
    boolean readResult = personFullInfo.readFullName(name);

    assertEquals(readResult, true);
    assertEquals(sn, personFullInfo.surname);
    assertEquals(fn, personFullInfo.firstName);
    assertEquals(mn, personFullInfo.middleName);
  }

  @Test
  public void readFullNameDoubleSn1() {
    String sn = "ЧИЛІН-ГІРІ";
    String fn = "Олександр";
    String mn = "Костянтинович";

    String name = sn + " " + fn + " " + mn;

    PersonFullInfo personFullInfo = new PersonFullInfo();
    boolean readResult = personFullInfo.readFullName(name);

    assertEquals(readResult, true);
    assertEquals(sn, personFullInfo.surname);
    assertEquals(fn, personFullInfo.firstName);
    assertEquals(mn, personFullInfo.middleName);
  }

  @Test
  public void readFullNameDoubleSn2() {
    String sn = "БОНДАРЧУК - ЛАДНА";
    String fn = "Олена";
    String mn = "Василівна";

    String name = sn + " " + fn + " " + mn;

    PersonFullInfo personFullInfo = new PersonFullInfo();
    boolean readResult = personFullInfo.readFullName(name);

    assertEquals(readResult, true);
    assertEquals(sn, personFullInfo.surname);
    assertEquals(fn, personFullInfo.firstName);
    assertEquals(mn, personFullInfo.middleName);
  }

  @Test
  public void readFullNameFalse1() {
    String sn = "БОНДАРЧУК - ЛАДНА";
    String fn = "Олена";
    String mn = "";

    String name = sn + " " + fn + " " + mn;

    PersonFullInfo personFullInfo = new PersonFullInfo();
    boolean readResult = personFullInfo.readFullName(name);

    assertEquals(readResult, false);
  }
}