package softballForce.app;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;
//
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/*
infos:

# comment line

 -
  surname: "ЧИЛІН-ГІРІ"
  firstName: "Олександр"
  middleName: "Костянтинович"
  dateOfBirth: "21.12.1965"
  organization: "Національна комісія з цінних паперів та фондового ринку"
  jobTitle: "член Національної комісії з цінних паперів та фондового ринку"
  checkResult: "Указ Президента України від 30 січня 2015 року №48/2015"
  term: "10 років"

 -
  surname: "Бондаренко"
  firstName: "Іван"
  middleName: "Петрович"
  dateOfBirth: "01.02.2095"
  organization: "Національна поліція"
  jobTitle: "Головний державний інспектор митного посту \"Тиса\" митниці ДФС"
  checkResult: "частина 8 статті 3 Закону України \"Про очищення влади\""
  term: "10 років"
 */
public class YamlInfoWriterTest {

  PersonFullInfo firstItem() {
    PersonFullInfo result = new PersonFullInfo();
    result.surname = "ЧИЛІН-ГІРІ";
    result.firstName = "Олександр";
    result.middleName = "Костянтинович";
    result.dateOfBirth = "21.12.1965";
    result.organization = "Національна комісія з цінних паперів та фондового ринку";
    result.jobTitle = "член Національної комісії з цінних паперів та фондового ринку";
    result.checkResult = "Указ Президента України від 30 січня 2015 року №48/2015";
    result.term = "10 років"    ;

    return result;
  }

  List<String> firstItemLines() {
    List<String> result = new LinkedList<>();

    result.add(" -");
    result.add("  surname: \"ЧИЛІН-ГІРІ\"");
    result.add("  firstName: \"Олександр\"");
    result.add("  middleName: \"Костянтинович\"");
    result.add("  dateOfBirth: \"21.12.1965\"");
    result.add("  organization: \"Національна комісія з цінних паперів та фондового ринку\"");
    result.add("  jobTitle: \"член Національної комісії з цінних паперів та фондового ринку\"");
    result.add("  checkResult: \"Указ Президента України від 30 січня 2015 року №48/2015\"");
    result.add("  term: \"10 років\"");
    result.add("");//this is not mandatory

    return result;
  }

  PersonFullInfo secondItem() {
    PersonFullInfo result = new PersonFullInfo();

    result.surname = "Бондаренко";
    result.firstName = "Іван";
    result.middleName = "Петрович";
    result.dateOfBirth = "01.02.2095";
    result.organization = "Національна поліція";
    result.jobTitle = "Головний державний інспектор митного посту \"Тиса\" митниці ДФС";
    result.checkResult = "частина 8 статті 3 Закону України \"Про очищення влади\"";
    result.term = "10 років";

    return result;
  }

  List<String> secondItemLines() {
    List<String> result = new LinkedList<>();

    result.add(" -");
    result.add("  surname: \"Бондаренко\"");
    result.add("  firstName: \"Іван\"");
    result.add("  middleName: \"Петрович\"");
    result.add("  dateOfBirth: \"01.02.2095\"");
    result.add("  organization: \"Національна поліція\"");
    result.add("  jobTitle: \"Головний державний інспектор митного посту \\\"Тиса\\\" митниці ДФС\"");
    result.add("  checkResult: \"частина 8 статті 3 Закону України \\\"Про очищення влади\\\"\"");
    result.add("  term: \"10 років\"");
    result.add("");//this is not mandatory

    return result;
  }

  @Test
  public void testToYaml() {
    List<PersonFullInfo> personFullInfos = new LinkedList<>();

    personFullInfos.add(firstItem());
    personFullInfos.add(secondItem());

    YamlInfoWriter yamlInfoWriter = new YamlInfoWriter();
    List<String> lines = yamlInfoWriter.toYaml(personFullInfos);

    assertNotNull(lines);

    List<String> expected = new LinkedList<>();
    expected.add("infos:");
    expected.addAll(firstItemLines());
    expected.addAll(secondItemLines());

    assertEquals(expected.size(), lines.size());

    ListIterator<String> eit = expected.listIterator();
    ListIterator<String> lit = lines.listIterator();
    while( eit.hasNext() && lit.hasNext() ) {
      String eline = eit.next();
      String line = lit.next();
      assertEquals(eline, line);
    }
  }

}