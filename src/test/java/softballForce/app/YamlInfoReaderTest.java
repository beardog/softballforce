package softballForce.app;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;
//
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/*
infos:

# comment line

 -
  surname: "ЧИЛІН-ГІРІ"
  firstName: "Олександр"
  middleName: "Костянтинович"
  dateOfBirth: "21.12.1965"
  organization: "Національна комісія з цінних паперів та фондового ринку"
  jobTitle: "член Національної комісії з цінних паперів та фондового ринку"
  checkResult: "Указ Президента України від 30 січня 2015 року №48/2015"
  term: "10 років"

 -
  surname: "Бондаренко"
  firstName: "Іван"
  middleName: "Петрович"
  dateOfBirth: "01.02.2095"
  organization: "Національна поліція"
  jobTitle: "Головний державний інспектор митного посту \"Тиса\" митниці ДФС"
  checkResult: "частина 8 статті 3 Закону України \"Про очищення влади\""
  term: "10 років"
 */
public class YamlInfoReaderTest {

  PersonFullInfo firstItem() {
    PersonFullInfo result = new PersonFullInfo();
    result.surname = "ЧИЛІН-ГІРІ";
    result.firstName = "Олександр";
    result.middleName = "Костянтинович";
    result.dateOfBirth = "21.12.1965";
    result.organization = "Національна комісія з цінних паперів та фондового ринку";
    result.jobTitle = "член Національної комісії з цінних паперів та фондового ринку";
    result.checkResult = "Указ Президента України від 30 січня 2015 року №48/2015";
    result.term = "10 років"    ;

    return result;
  }

  List<String> firstItemLines() {
    List<String> result = new LinkedList<>();

    result.add(" -");
    result.add("  surname: \"ЧИЛІН-ГІРІ\"");
    result.add("  firstName: \"Олександр\"");
    result.add("  middleName: \"Костянтинович\"");
    result.add("  dateOfBirth: \"21.12.1965\"");
    result.add("  organization: \"Національна комісія з цінних паперів та фондового ринку\"");
    result.add("  jobTitle: \"член Національної комісії з цінних паперів та фондового ринку\"");
    result.add("  checkResult: \"Указ Президента України від 30 січня 2015 року №48/2015\"");
    result.add("  term: \"10 років\"");
    result.add("");//this is not mandatory

    return result;
  }

  PersonFullInfo secondItem() {
    PersonFullInfo result = new PersonFullInfo();

    result.surname = "Бондаренко";
    result.firstName = "Іван";
    result.middleName = "Петрович";
    result.dateOfBirth = "01.02.2095";
    result.organization = "Національна поліція";
    result.jobTitle = "Головний державний інспектор митного посту \"Тиса\" митниці ДФС";
    result.checkResult = "частина 8 статті 3 Закону України \"Про очищення влади\"";
    result.term = "10 років";

    return result;
  }

  List<String> secondItemLines() {
    List<String> result = new LinkedList<>();

    result.add(" -");
    result.add("  surname: \"Бондаренко\"");
    result.add("  firstName: \"Іван\"");
    result.add("  middleName: \"Петрович\"");
    result.add("  dateOfBirth: \"01.02.2095\"");
    result.add("  organization: \"Національна поліція\"");
    result.add("  jobTitle: \"Головний державний інспектор митного посту \\\"Тиса\\\" митниці ДФС\"");
    result.add("  checkResult: \"частина 8 статті 3 Закону України \\\"Про очищення влади\\\"\"");
    result.add("  term: \"10 років\"");
    result.add("");//this is not mandatory

    return result;
  }

  @Test
  public void testProcessLines() {

    List<String> lines = new LinkedList<>();
    lines.add("infos:");
    lines.addAll(firstItemLines());
    lines.addAll(secondItemLines());

    YamlInfoReader yamlInfoReader = new YamlInfoReader();
    List<PersonFullInfo> personFullInfos = yamlInfoReader.processLines(lines);

    assertNotNull(personFullInfos);

    List<PersonFullInfo> expected = new LinkedList<>();
    expected.add(firstItem());
    expected.add(secondItem());

    assertEquals(expected.size(), personFullInfos.size());

    ListIterator<PersonFullInfo> eit = expected.listIterator();
    ListIterator<PersonFullInfo> rit = personFullInfos.listIterator();
    while( eit.hasNext() && rit.hasNext() ) {
      PersonFullInfo e = eit.next();
      PersonFullInfo r = rit.next();
      assertEquals(e.surname, r.surname);
      assertEquals(e.firstName, r.firstName);
      assertEquals(e.middleName, r.middleName);
      assertEquals(e.dateOfBirth, r.dateOfBirth);
      assertEquals(e.organization, r.organization);
      assertEquals(e.jobTitle, r.jobTitle);
      assertEquals(e.checkResult, r.checkResult);
      assertEquals(e.term, r.term);
    }
  }

}