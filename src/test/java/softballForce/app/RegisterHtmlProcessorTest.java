package softballForce.app;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;
//
import java.util.LinkedList;
import java.util.List;

public class RegisterHtmlProcessorTest {

  @Test
  public void testMakeOneLineBasic() {
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    List<String> ls =  new LinkedList<>();
    ls.add("One");
    ls.add("Two");
    String line = rhp.makeOneLine(ls);

    assertEquals(true, line.equals("OneTwo"));
  }

  @Test
  public void testMakeOneLineEmptyLine() {
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    List<String> ls = new LinkedList<>();
    ls.add("One");
    ls.add("");
    ls.add("Two");
    String line = rhp.makeOneLine(ls);

    assertEquals(true, line.equals("OneTwo"));
  }

  @Test
  public void testMakeOneLineAllEmpty() {
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    List<String> ls = new LinkedList<>();
    String line = rhp.makeOneLine(ls);

    assertEquals(true, line.isEmpty());
  }

  @Test
  public void testMakeOneLineEmptyNewLine() {
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    List<String> ls = new LinkedList<>();
    ls.add("One");
    ls.add("\n");
    ls.add("Two");
    String line = rhp.makeOneLine(ls);

    assertEquals(true, line.equals("OneTwo"));
  }

  @Test
  public void testMakeOneLineBasicNewLine() {
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    List<String> ls =  new LinkedList<>();
    ls.add("One\n");
    ls.add("Two");
    String line = rhp.makeOneLine(ls);

    assertEquals(true, line.equals("OneTwo"));
  }

  @Test
  public void testMakeOneLineBasicNewLine2() {
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    List<String> ls =  new LinkedList<>();
    ls.add("One\n");
    ls.add("Two\n");
    String line = rhp.makeOneLine(ls);

    assertEquals(true, line.equals("OneTwo"));
  }

  @Test
  public void testMakeOneLineEmptyNewLineLast() {
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    List<String> ls = new LinkedList<>();
    ls.add("One");
    ls.add("Two");
    ls.add("\n");
    String line = rhp.makeOneLine(ls);

    assertEquals(true, line.equals("OneTwo"));
  }

  // =========================================================================

  @Test
  public void testGetSpanLinesBasic() {
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    String ln = "";
    ln += "    <div class=\"bs-component\">";
    String lineToFind = "<span class=\"s-name\" data_name=\"АНДРЄЄВ Денис Миколайович\" data_date=\"21.12.1977\">8. АНДРЄЄВ Денис Миколайович</span>";
    ln += lineToFind;
    ln += "            </div>";

    List<String> lines = rhp.getSpanLines(ln);

    assertEquals(1, lines.size());
    assertEquals(true, lines.get(0).equals(lineToFind));
  }

  @Test
  public void testGetSpanLinesBasicDouble() {
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    String ln = "";
    ln += "    <div class=\"bs-component\">";
    String lineToFind = "<span class=\"s-name\" data_name=\"АНДРЄЄВ Денис Миколайович\" data_date=\"21.12.1977\">8. АНДРЄЄВ Денис Миколайович</span>";
    ln += lineToFind;
    String lineToFind2 = "<span class=\"s-name\" data_name=\"БАЗАЛЄВ Григорій Корнійович\" data_date=\"20.06.1962\">19. БАЗАЛЄВ Григорій Корнійович</span>";
    ln += lineToFind2;
    ln += "            </div>";

    List<String> lines = rhp.getSpanLines(ln);

    assertEquals(2, lines.size());
    assertEquals(true, lines.get(0).equals(lineToFind));
    assertEquals(true, lines.get(1).equals(lineToFind2));
  }


  @Test
  public void testGetSpanLinesNothingToFind() {
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    String ln = "";
    ln += "    <div class=\"bs-component\">";
    ln += "            </div>";

    List<String> lines = rhp.getSpanLines(ln);

    assertEquals(0, lines.size());
  }

  @Test
  public void testGetSpanLinesEmptyLine() {
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    String ln = "";
    List<String> lines = rhp.getSpanLines(ln);

    assertEquals(0, lines.size());
  }

  @Test
  public void testGetSpanLinesGreedy() {
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    String ln = "";
    ln += "    <div class=\"bs-component\">";
    String lineToFind = "<span class=\"s-name\" data_name=\"АНДРЄЄВ Денис Миколайович\" data_date=\"21.12.1977\">8. АНДРЄЄВ Денис Миколайович</span>";
    ln += lineToFind;
    ln += "<span class=\"icon-bar\"></span>";
    ln += "            </div>";

    List<String> lines = rhp.getSpanLines(ln);

    assertEquals(1, lines.size());
    assertEquals(true, lines.get(0).equals(lineToFind));
  }

  // =========================================================================

  @Test
  public void testReadSpanLineBasicEn() {
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    String sn = "Aaaaaaa";
    String fn = "Bbb";
    String mn = "Cccccccccc";

    String ln = "";
    ln += "<span class=\"s-name\" data_name=\"" + sn + " " + fn + " " + mn + "\" ";
    ln += "data_date=\"21.12.1977\">8. " + sn + " " + fn + " " + mn + "</span>";

    PersonInfo pninfo = rhp.readSpanLine(ln);

    assertNotNull(pninfo);
    assertEquals(sn, pninfo.surname);
    assertEquals(fn, pninfo.firstName);
    assertEquals(mn, pninfo.middleName);
    assertEquals("21.12.1977", pninfo.dateOfBirth);
  }

  @Test
  public void testReadSpanLineBasicUk() {
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    String sn = "БОНДАРЕНКО";
    String fn = "Олександр";
    String mn = "Миколайович";

    String ln = "";
    ln += "<span class=\"s-name\" data_name=\"" + sn + " " + fn + " " + mn + "\" ";
    ln += "data_date=\"21.12.1977\">8. " + sn + " " + fn + " " + mn + "</span>";

    PersonInfo pninfo = rhp.readSpanLine(ln);

    assertNotNull(pninfo);
    assertEquals(sn, pninfo.surname);
    assertEquals(fn, pninfo.firstName);
    assertEquals(mn, pninfo.middleName);
    assertEquals("21.12.1977", pninfo.dateOfBirth);
  }

  @Test
  public void testReadSpanLineBasic2() {
    // changed number of  spaces between some words
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    String sn = "БОНДАРЕНКО";
    String fn = "Олександр";
    String mn = "Миколайович";

    String ln = "";
    ln += "<span class=\"s-name\" data_name=\"" + sn + " " + fn + " " + mn + "\" ";
    ln += "data_date=\"21.12.1977\">80. " + sn + "  " + fn + " " + mn + "</span>";

    PersonInfo pninfo = rhp.readSpanLine(ln);

    assertNotNull(pninfo);
    assertEquals(sn, pninfo.surname);
    assertEquals(fn, pninfo.firstName);
    assertEquals(mn, pninfo.middleName);
    assertEquals("21.12.1977", pninfo.dateOfBirth);
  }

  @Test
  public void testReadSpanLineBasic3() {
    // changed number of  spaces between some words
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    String sn = "БОНДАРЕНКО";
    String fn = "Олександр";
    String mn = "Миколайович";

    String ln = "";
    ln += "<span class=\"s-name\" data_name=\"" + sn + " " + fn + "  " + mn + "\" ";
    ln += "data_date=\"21.12.1977\">806. " + sn + " " + fn + "  " + mn + "</span>";

    PersonInfo pninfo = rhp.readSpanLine(ln);

    assertNotNull(pninfo);
    assertEquals(sn, pninfo.surname);
    assertEquals(fn, pninfo.firstName);
    assertEquals(mn, pninfo.middleName);
    assertEquals("21.12.1977", pninfo.dateOfBirth);
  }

  @Test
  public void testReadSpanLineDotAfterName() {
    // changed number of  spaces between some words
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    String sn = "БОНДАРЕНКО";
    String fn = "Олександр";
    String mn = "Миколайович";

    String ln = "";
    ln += "<span class=\"s-name\" data_name=\"" + sn + " " + fn + "  " + mn + ".\" ";
    ln += "data_date=\"21.12.1977\">806. " + sn + " " + fn + "  " + mn + ".</span>";

    PersonInfo pninfo = rhp.readSpanLine(ln);

    assertNotNull(pninfo);
    assertEquals(sn, pninfo.surname);
    assertEquals(fn, pninfo.firstName);
    assertEquals(mn, pninfo.middleName);
    assertEquals("21.12.1977", pninfo.dateOfBirth);
  }

  @Test
  public void testReadSpanLineSpaceAfterName() {
    // changed number of  spaces between some words
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    String sn = "БОНДАРЕНКО";
    String fn = "Олександр";
    String mn = "Миколайович";

    String ln = "";
    ln += "<span class=\"s-name\" data_name=\"" + sn + " " + fn + "  " + mn + " \" ";
    ln += "data_date=\"21.12.1977\">806. " + sn + " " + fn + "  " + mn + " </span>";

    PersonInfo pninfo = rhp.readSpanLine(ln);

    assertNotNull(pninfo);
    assertEquals(sn, pninfo.surname);
    assertEquals(fn, pninfo.firstName);
    assertEquals(mn, pninfo.middleName);
    assertEquals("21.12.1977", pninfo.dateOfBirth);
  }

  @Test
  public void testReadSpanLineDoubleName() {
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    String sn = "БОНДАРЧУК - ЛАДНА";
    String fn = "Олена";
    String mn = "Василівна";

    String ln = "";
    ln += "<span class=\"s-name\" data_name=\"" + sn + " " + fn + " " + mn + "\" ";
    ln += "data_date=\"21.12.1977\">8. " + sn + " " + fn + " " + mn + "</span>";

    PersonInfo pninfo = rhp.readSpanLine(ln);

    assertNotNull(pninfo);
    assertEquals(sn, pninfo.surname);
    assertEquals(fn, pninfo.firstName);
    assertEquals(mn, pninfo.middleName);
    assertEquals("21.12.1977", pninfo.dateOfBirth);
  }

  @Test
  public void testReadSpanLineDoubleName2() {
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    String sn = "ЧИЛІН-ГІРІ";
    String fn = "Олександр";
    String mn = "Костянтинович";

    String ln = "";
    ln += "<span class=\"s-name\" data_name=\"" + sn + " " + fn + " " + mn + "\" ";
    ln += "data_date=\"21.12.1977\">8. " + sn + " " + fn + " " + mn + "</span>";

    PersonInfo pninfo = rhp.readSpanLine(ln);

    assertNotNull(pninfo);
    assertEquals(sn, pninfo.surname);
    assertEquals(fn, pninfo.firstName);
    assertEquals(mn, pninfo.middleName);
    assertEquals("21.12.1977", pninfo.dateOfBirth);
  }

  @Test
  public void testReadSpanLineApos() {
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    String sn = "НЕТРЕБ'ЯК";
    String fn = "Юрій";
    String mn = "Іванович";

    String ln = "";
    ln += "<span class=\"s-name\" data_name=\"" + sn + " " + fn + " " + mn + "\" ";
    ln += "data_date=\"21.12.1977\">8. " + sn + " " + fn + " " + mn + "</span>";

    PersonInfo pninfo = rhp.readSpanLine(ln);

    assertNotNull(pninfo);
    assertEquals(sn, pninfo.surname);
    assertEquals(fn, pninfo.firstName);
    assertEquals(mn, pninfo.middleName);
    assertEquals("21.12.1977", pninfo.dateOfBirth);
  }

  @Test
  public void testReadSpanLineNoMatch() {
    RegisterHtmlProcessor rhp = new RegisterHtmlProcessor();

    String ln = "foo bar";
    PersonInfo pninfo = rhp.readSpanLine(ln);

    assertNull(pninfo);
  }

  // =========================================================================
}
