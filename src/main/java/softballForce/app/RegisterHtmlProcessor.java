package softballForce.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//
import java.util.LinkedList;
import java.util.List;
//
import java.util.regex.Pattern;
import java.util.regex.Matcher;


/**
 *
 */
public class RegisterHtmlProcessor {

  private static final Logger logger = LoggerFactory.getLogger("softballForce.main");

  public RegisterHtmlProcessor() {
    //nothing to do here
  }

  protected String errorMessage = "";

  /**
   *   Returns error message.
   *   Also returns empty string if no error happened.
   * @return error message, or empty string
  */
  public String getErrorMessage() {
    return errorMessage;
  }

  /**
   *   Searches full big line for a specific substrings.
   *   It searches for lines like
   * <pre>
   * {@code
   *     <span class="s-name" data_name="БАГРІЙЧУК Ігор Володимирович"
   *     data_date="22.01.1972">18. БАГРІЙЧУК Ігор Володимирович</span>
   * }
   * </pre>
   * @param bigLine all html content in one line
   * @return the list of found substrings
   */
  List<String> getSpanLines(String bigLine) {
    List<String> result = new LinkedList<>();

    String pstr = "(<span\\s+class=\"s-name\".+?</span>)";

    Pattern pattern = Pattern.compile(pstr);
    Matcher matcher = pattern.matcher(bigLine);

    while(matcher.find()) {
      result.add(matcher.group());
    }

    return result;
  }

  /**
   *  Takes list of lines, joins them into one long string.
   *  Removes trailing newline characters ("\n") from incoming lines.
   * @param lines Some lines
   * @return joined string
   */
  String makeOneLine(List<String> lines) {
    String result = "";
    for(String ss : lines) {
      String tmps = ss;
      if (ss.endsWith("\n")) {
        tmps = ss.substring(0, ss.length()-1);
      }
      result += tmps;
    }

    return result;
  }

  protected String lastNumberProcessed="0";

  /**
   *   Reads person information from given html piece.
   *   Expects input to be like
   * <pre>
   * {@code
   *     <span class="s-name" data_name="АДАМОВИЧ Олена Євгенівна"
   *     data_date="06.05.1962">2. АДАМОВИЧ Олена Євгенівна</span>
   * }
   * </pre>
   * @param line incoming line
   * @return fetched info or null if error appears
   */
  PersonInfo readSpanLine(String line) {
    PersonInfo result = new PersonInfo();

    String pstr = "";
    pstr += "^<span class=\"s-name\" data_name=\""; // common start

    //three groups for surname, first name and middle name
    //note the first name can consist of two words, separated by "-"
    // like "БОНДАРЧУК - ЛАДНА"
    //note some name can contain "'" sign (apostrophe), just copied it from file
    pstr += "([\\p{L}']+\\s?\\-?\\s?[\\p{L}']+)\\s+([\\p{L}']+)\\s+([\\p{L}']+)";

    pstr += "[\\s\\.]*\"\\s+data_date=\""; // intermediate stuff
    pstr += "(\\d\\d\\.\\d\\d\\.\\d\\d\\d\\d)"; // date, another group

    // intermediate stuff. Also one group for number in the list, it will
    // be used to check processing results
    pstr += "\">\\s*(\\d+)\\.\\s*";

    // again three groups for names
    pstr += "([\\p{L}']+\\s?\\-?\\s?[\\p{L}']+)\\s+([\\p{L}']+)\\s+([\\p{L}']+)";

    //sometimes there is a dot or space after name
    pstr += "[\\s\\.]*</span>$"; // end stuff

    Pattern pattern = Pattern.compile(pstr);
    Matcher matcher = pattern.matcher(line);

    if (!matcher.find()) {
      return null;
    }

    result.surname=matcher.group(1);
    result.firstName=matcher.group(2);
    result.middleName = matcher.group(3);
    result.dateOfBirth = matcher.group(4);

    lastNumberProcessed=matcher.group(5);

    String tmps;
    tmps = matcher.group(6);
    if (!tmps.equals(result.surname)) {
      logger.warn("Regex: in {} got problem with {} and {}",
              line, tmps, result.surname);
    }

    tmps = matcher.group(7);
    if (!tmps.equals(result.firstName)) {
      logger.warn("Regex: in {} got problem with 1stname {} and {}",
              line, tmps, result.firstName);
    }

    tmps = matcher.group(8);
    if (!tmps.equals(result.middleName)) {
      logger.warn("Regex: in {} got problem with m-name {} and {}",
              line, tmps, result.middleName);
    }

    return result;
  }

  /**
   *   Processes html page content into a list of person info objects.
   * @param rawContent
   * @return list of objects or null if any error appears
   */
  public List<PersonInfo> process(List<String> rawContent) {

    String oneLine = makeOneLine(rawContent);
    if (oneLine.isEmpty()) {
      errorMessage = "Received no content";
      return null;
    }

    List <String> htmlContent = getSpanLines(oneLine) ;
    if (htmlContent.isEmpty()) {
      errorMessage = "Failed to find any info";
      return null;
    }

    List<PersonInfo> result = new LinkedList<>();
    lastNumberProcessed="0"; //just to be sure

    for(String spanLine: htmlContent) {
      PersonInfo pnInfo = readSpanLine(spanLine);
      if (pnInfo!=null) {
        result.add(pnInfo);
      }
      else {
        logger.warn("Incorrect span line {}", spanLine);
      }
    }

    String tmps = String.format("%d", result.size());
    if (!tmps.equals(lastNumberProcessed)) {
      logger.warn("Seems like some names were missed ({}/{})", tmps, lastNumberProcessed);
    }

    return result;
  }

}
