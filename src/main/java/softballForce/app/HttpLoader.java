package softballForce.app;

import java.util.List;

public interface HttpLoader {
  boolean setUrl(String url);

  List<PersonInfo> load();

  String getErrorMessage();
}
