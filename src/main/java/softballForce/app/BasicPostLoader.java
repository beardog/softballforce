package softballForce.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class BasicPostLoader {

  protected static final Logger logger = LoggerFactory.getLogger("softballForce.main");

  protected String errorMessage = "";

  /**
   * Basic url to get info.
   * It's a folder name in this fake implementation.
   */
  protected String basicUrl = "";

  public BasicPostLoader(String basicUrl) {
    this.basicUrl = basicUrl;
  }

  /**
   *
   */
  public String getErrorMessage() {
    return errorMessage;
  }

  /**
   *
   * @return Html content to be processed with other methods
   */
  protected abstract List<String> getHtmlContent(String name, String date) ;


  /**
   *   Searches full big line for a specific substrings.
   *   It searches for lines like
   * <pre>
   * {@code
   *     <td>.*</td>
   * }
   * </pre>
   * which are table cells with any content
   *
   * @param bigLine all html content in one line
   * @return the list of found substrings
   */
  List<String> getTableDataLines(String bigLine) {
    List<String> result = new LinkedList<>();

    String pstr = "(<td>.*?</td>)";

    Pattern pattern = Pattern.compile(pstr);
    Matcher matcher = pattern.matcher(bigLine);

    while(matcher.find()) {
      result.add(matcher.group());
    }

    return result;
  }

  /**
   *  Takes list of lines, joins them into one long string.
   *  Removes trailing newline characters ("\n") from incoming lines.
   * @param lines Some lines
   * @return joined string
   */
  String makeOneLine(List<String> lines) {
    String result = "";
    for(String ss : lines) {
      String tmps = ss;
      if (ss.endsWith("\n")) {
        tmps = ss.substring(0, ss.length()-1);
      }
      result += tmps;
    }

    return result;
  }

  /**
   *   Clears received lines from <td> </td> tags.
   * @param tdLines
   * @return
   */
  protected List<String> clearDataLines(List<String> tdLines) {
    List<String> result = new LinkedList<>();

    String patternString = "^<td>(.*)</td>$"; //
    Pattern pattern = Pattern.compile(patternString);

    for(String tableDataLine: tdLines) {
      Matcher matcher = pattern.matcher(tableDataLine);
      if (matcher.find()) {
        result.add(matcher.group(1).trim());//note, string gets trimmed here
      }
      else {
        logger.warn("Incorrect line {}", tableDataLine);
      }
    }

    return result;
  }

  protected List<String> getTableData(String name, String date) {
    //First, get all the html content
    List<String> htmlContent = getHtmlContent(name, date);

    // Join all string into one line
    String oneLine = makeOneLine(htmlContent);
    if (oneLine.isEmpty()) {
      errorMessage = "Received no content";
      return null;
    }

    List<String> tableDataLines = getTableDataLines(oneLine) ;
    if (tableDataLines.isEmpty()) {
      errorMessage = "Failed to find any info";
      return null;
    }

    List<String> result = clearDataLines(tableDataLines);

    return result;
  }

  /**
   *   Loads detailed information about person.
   * @param info
   * @return Full info about person or null if some error
   */
  public PersonFullInfo load(PersonInfo info) {
    PersonFullInfo result = new PersonFullInfo();
    result.surname = info.surname;
    result.firstName = info.firstName;
    result.middleName = info.middleName;
    result.dateOfBirth = info.dateOfBirth;

    String fullName = String.format("%s %s %s",
                                 info.surname, info.firstName, info.middleName);
    List<String> tableData = getTableData(fullName, info.dateOfBirth);
    if (tableData==null) {
      return null;
    }

    if (tableData.size()!=5) {
      errorMessage = String.format("Incorrect number of lines in data (%d, 5)",
              tableData.size());
      return null;
    }

    //tableData.get(0) is person name, we already have it
    result.organization = tableData.get(1);
    result.jobTitle =  tableData.get(2);
    result.checkResult =  tableData.get(3);
    result.term = tableData.get(4);

    return result;
  }


}
