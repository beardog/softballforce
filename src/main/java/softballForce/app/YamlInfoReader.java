package softballForce.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 */
public class YamlInfoReader {
  protected static final Logger logger = LoggerFactory.getLogger("softballForce.main");

  protected String errorMessage = "";

  /**
   *
   */
  public String getErrorMessage() {
    return errorMessage;
  }

  List<String> readFile(String filename) {
    List<String> result = new LinkedList<>() ;

    try {
      FileReader fileReader = new FileReader(filename);
      BufferedReader bufferedReader = new BufferedReader(fileReader);

      String line ;
      while((line = bufferedReader.readLine()) != null) {
        result.add(line);
      }

      bufferedReader.close();
    }
    catch(IOException ex) {
      errorMessage = "Error reading file '" + filename + "'";
      errorMessage += "("+ ex.getMessage() +")";
      return null;
    }

    return result;
  }

  String unescape(String str) {
    String result = str;
    result = result.replace("\\\"", "\"");
    return result;
  }

  List<PersonFullInfo> processLines(List<String> lines) {
    List<PersonFullInfo> result = null;

    if (lines.size()<2) {
      return null;
    }


    String line = lines.get(0);
    if (!line.equals("infos:")) {
      errorMessage = "Incorrect yaml start";
      return null;
    }

    PersonFullInfo personFullInfo = null;

    Pattern surnamePattern = Pattern.compile("^  surname: \"(.*)\"\\s*#*.*$");
    Pattern firstNamePattern = Pattern.compile("^  firstName: \"(.*)\"\\s*#*.*$");
    Pattern middleNamePattern = Pattern.compile("^  middleName: \"(.*)\"\\s*#*.*$");
    Pattern dateOfBirthPattern = Pattern.compile("^  dateOfBirth: \"(.*)\"\\s*#*.*$");
    Pattern organizationPattern = Pattern.compile("^  organization: \"(.*)\"\\s*#*.*$");
    Pattern jobTitlePattern = Pattern.compile("^  jobTitle: \"(.*)\"\\s*#*.*$");
    Pattern checkResultPattern = Pattern.compile("^  checkResult: \"(.*)\"\\s*#*.*$");
    Pattern termPattern = Pattern.compile("^  term: \"(.*)\"\\s*#*.*$");

    result = new LinkedList<>();

    ListIterator<String> lineIter = lines.listIterator();
    while (lineIter.hasNext()) {
      line = lineIter.next();

      if (line.isEmpty()) {
        continue;
      }

      if( line.startsWith("#")) {
        continue;
      }

      if (line.equals(" -")) {
        if (personFullInfo!=null) {
          result.add(personFullInfo);
        }
        personFullInfo = new PersonFullInfo();
        continue;
      }

      Matcher surnameMatcher = surnamePattern.matcher(line);
      if (surnameMatcher.find()){
        personFullInfo.surname = unescape(surnameMatcher.group(1) );
        continue;
      }

      Matcher firstNameMatcher = firstNamePattern.matcher(line);
      if (firstNameMatcher.find()){
        personFullInfo.firstName = unescape( firstNameMatcher.group(1) );
        continue;
      }

      Matcher middleNameMatcher = middleNamePattern.matcher(line);
      if (middleNameMatcher.find()){
        personFullInfo.middleName = unescape( middleNameMatcher.group(1) );
        continue;
      }

      Matcher dateOfBirthMatcher =  dateOfBirthPattern.matcher(line);
      if (dateOfBirthMatcher.find()){
        personFullInfo.dateOfBirth = unescape( dateOfBirthMatcher.group(1) );
        continue;
      }

      Matcher organizationMatcher = organizationPattern.matcher(line);
      if (organizationMatcher.find()){
        personFullInfo.organization = unescape( organizationMatcher.group(1) );
        continue;
      }

      Matcher jobTitleMatcher = jobTitlePattern.matcher(line);
      if (jobTitleMatcher.find()){
        personFullInfo.jobTitle = unescape( jobTitleMatcher.group(1) );
        continue;
      }

      Matcher checkResultMatcher = checkResultPattern.matcher(line);
      if (checkResultMatcher.find()){
        personFullInfo.checkResult = unescape( checkResultMatcher.group(1) );
        continue;
      }

      Matcher termMatcher = termPattern.matcher(line);
      if (termMatcher.find()){
        personFullInfo.term = unescape( termMatcher.group(1) );
        continue;
      }
    }

    if (personFullInfo!=null) {
      result.add(personFullInfo);
    }


    return result;
  }

  public List<PersonFullInfo> readFromFile(String filename) {
    List<PersonFullInfo> result = null;

    List<String> lines = readFile(filename) ;
    if (lines == null) {
      return null;
    }

    result = processLines(lines);

    return result;
  }

}