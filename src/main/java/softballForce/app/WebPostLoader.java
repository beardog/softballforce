package softballForce.app;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.io.BufferedReader;
import java.io.Reader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.zip.GZIPInputStream;


/**
 *    Fake implementation that gets some detailed person information for
 *  integration tests.
 *    Actually it will try to read one of two possible files; files should
 *  be named as #addUrlC1 and @addUrlC2.
 *
 */
public class WebPostLoader extends BasicPostLoader {

  protected String addUrl = "";

  public WebPostLoader(String basicUrl) {
    super(basicUrl);
  }

  private List<String> sendPost(String name, String date) throws Exception {
    URL obj = new URL(basicUrl);
    HttpURLConnection con = (HttpURLConnection) obj.openConnection();

    //add reqqest header
    con.setRequestMethod("POST");
    con.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:44.0) Gecko/20100101 Firefox/44.0");
    con.setRequestProperty("Accept", "*/*");
    con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
    con.setRequestProperty("Accept-Encoding", "gzip, deflate");
    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    con.setRequestProperty("X-Requested-With", "XMLHttpRequest");

    con.setRequestProperty("Referer", "http://lustration.minjust.gov.ua/register");

    String urlParameters = String.format("fio=%s&date=%s",
            URLEncoder.encode(name, "UTF-8"), URLEncoder.encode(date, "UTF-8"));

    // Send post request
    con.setDoOutput(true);
    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
    wr.writeBytes(urlParameters);
    wr.flush();
    wr.close();

    int responseCode = con.getResponseCode();
    if (responseCode!=200) {
      errorMessage = String.format("Got wrong response code \"%d\"", responseCode);
      return null;
    }
    //System.out.println("\nSending 'POST' request to URL : " + url);
    logger.debug("Post parameters : {}", urlParameters);
    logger.debug("Response Code : {}", responseCode);

    Reader reader = null;
    if ("gzip".equals(con.getContentEncoding())) {
      reader = new InputStreamReader(new GZIPInputStream(con.getInputStream()));
    }
    else {
      reader = new InputStreamReader(con.getInputStream());
    }

    BufferedReader in = new BufferedReader(reader);
    String inputLine;

    List<String> result = new LinkedList<>();
    logger.trace("Result is");
    while ((inputLine = in.readLine()) != null) {
      result.add(inputLine);
      logger.trace(inputLine);
    }
    in.close();

    //print result
    return result;
  }

  protected List<String> getHtmlContent(String name, String date) {
    List<String> loadedLines = null ;

    logger.debug("getHtmlContent: Loading " + name);

    try {
      loadedLines = sendPost(name, date);
    }
    catch(Exception ex) {
      errorMessage = "Error reading info on some person. ";
      errorMessage += "("+ ex.getMessage() +")";
      return null;
    }

    return loadedLines;
  }


}
