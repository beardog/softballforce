package softballForce.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;


/**
 *
 */
public class FakeHttpLoader extends BasicHttpLoader{

  protected List<String> getRawContent() {
    List<String> result = new LinkedList<>() ;

    try {
      FileReader fileReader = new FileReader(targetUrl);
      BufferedReader bufferedReader = new BufferedReader(fileReader);

      String line ;
      while((line = bufferedReader.readLine()) != null) {
        result.add(line);
      }

      bufferedReader.close();
    }
    catch(IOException ex) {
      errorMessage = "Error reading file '" + targetUrl + "'";
      errorMessage += "("+ ex.getMessage() +")";
      return null;
    }

    return result;
  }

}
