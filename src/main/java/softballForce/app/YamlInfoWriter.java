package softballForce.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 */
public class YamlInfoWriter {

  protected static final Logger logger = LoggerFactory.getLogger("softballForce.main");

  protected String errorMessage = "";

  /**
   *
   */
  public String getErrorMessage() {
    return errorMessage;
  }

  String addEscape(String str) {
    String result = str;
    result = result.replace("\"", "\\\"");
    return result;
  }

  /**  Prepares YAML representation of given data.
   *   Each string corresponds to one line of YAML file. Strings do not have
   * trailing "\n" character.
   * @param infos
   * @return list of strings representing data in YAML format
   */
  List<String> toYaml(List<PersonFullInfo> infos) {
    List<String> result = new LinkedList<>();

    result.add("infos:");

    for(PersonFullInfo personFullInfo: infos) {
      result.add(" -") ;
      result.add("  surname: \"" + addEscape(personFullInfo.surname) + "\"");
      result.add("  firstName: \"" + addEscape(personFullInfo.firstName) + "\"");
      result.add("  middleName: \"" + addEscape(personFullInfo.middleName) + "\"");
      result.add("  dateOfBirth: \"" + addEscape(personFullInfo.dateOfBirth) + "\"");
      result.add("  organization: \"" + addEscape(personFullInfo.organization) + "\"");
      result.add("  jobTitle: \"" + addEscape(personFullInfo.jobTitle) + "\"");
      result.add("  checkResult: \"" + addEscape(personFullInfo.checkResult) + "\"");
      result.add("  term: \"" + addEscape(personFullInfo.term) + "\"");
      result.add("");
    }

    return result;
  }

  /** Saves given info to specified YAML file.
   *  Overwrites file if it exists. Does not use any third party libraries
   * @param infos  Information to save
   * @param filename File to write.
   * @return true on success, false otherwise
   */
  public boolean saveToFile(List<PersonFullInfo> infos, String filename) {
    List<String> lines = toYaml(infos);
    if (lines==null) {
      return false;
    }

    boolean result = true;
    BufferedWriter writer = null;
    try {
      writer = new BufferedWriter(new FileWriter(filename));
      for(String line:lines) {
        writer.write(line);
        writer.newLine();
      }
    } catch (IOException e) {
      errorMessage = e.getMessage();
      result = false;
    } finally {
      try {
        // Close the writer regardless of what happens...
        writer.close();
      } catch (IOException e) {
        result = false;
        errorMessage = e.getMessage();
      } catch (NullPointerException e) {
        result = false;
        errorMessage = "Strange NPE";
      }
    }

    return result;
  }
}