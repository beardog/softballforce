package softballForce.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
//
import java.io.BufferedReader;
import java.io.Reader;
import java.util.zip.GZIPInputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


/**  Real implementation of BasicHttpLoader.
 *   Sends
 */
public class WebHttpLoader extends BasicHttpLoader{

  /** Sends GET request to @targetUrl.
   *   Adds response content as strings to given lines parameter. Sets @errorMessage
   *  if some error appears
   * @return  true on success
   * @throws Exception
   */
  protected List<String> sendGet() throws Exception {
    URL obj = new URL(targetUrl);
    HttpURLConnection con = (HttpURLConnection) obj.openConnection();

    // optional default is GET
    con.setRequestMethod("GET");

    //add request header
    con.setRequestProperty("Accept-Encoding", "gzip, deflate");
    con.setRequestProperty("User-Agent",
            "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:44.0) Gecko/20100101 Firefox/44.0");

    int responseCode = con.getResponseCode();
    //System.out.println("\nSending 'GET' request to URL : " + url);
    logger.debug("Response Code : {}" , responseCode);

    if (responseCode!=200) {
      errorMessage = String.format("Got wrong response code \"%d\"", responseCode);
      return null;
    }

    Reader reader = null;
    if ("gzip".equals(con.getContentEncoding())) {
      reader = new InputStreamReader(new GZIPInputStream(con.getInputStream()));
    }
    else {
      reader = new InputStreamReader(con.getInputStream());
    }

    BufferedReader in = new BufferedReader( reader );
    String inputLine;

    List<String> result = new LinkedList<>();
    while ((inputLine = in.readLine()) != null) {
      result.add(inputLine);
    }
    in.close();

    return result ;
  }

  protected List<String> getRawContent() {
    List<String> result = null;

    try {
      result = sendGet() ;
    }
    catch(Exception ex) {
      errorMessage = "Error reading file '" + targetUrl + "'";
      errorMessage += "("+ ex.getMessage() +")";
      return null;
    }

    return result;
  }

}
