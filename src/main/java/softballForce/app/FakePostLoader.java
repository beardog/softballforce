package softballForce.app;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;

/**
 *    Fake implementation that gets some detailed person information for
 *  integration tests.
 *    Actually it will try to read one of two possible files; files should
 *  be named as #addUrlC1 and @addUrlC2.
 *
 */
public class FakePostLoader extends BasicPostLoader {

  protected static String addUrlC1 = "response.html";
  protected static String addUrlC2 = "response2.html";

//  static String tableHeaderName = "Прізвище, ім'я, по батькові";
//  static String tableHeaderJobTitle = "Посада на час застосування положення Закону України «Про очищення влади»";
//  static String tableHeaderOrganization = "Місце роботи";
//  static String tableHeaderCheckResult = "Відомості про результати перевірки";
//  static String tableHeaderTerm = "Час протягом якого на особу поширюється заборона, передбачена Законом України "Про очищення влади"";

  protected String addUrl = "";

  public FakePostLoader(String basicUrl) {
    super(basicUrl);
  }

  protected List<String> getHtmlContent(String name, String date) {
    List<String> loadedLines = new LinkedList<>() ;

    String targetUrl = basicUrl + "/" + getAddUrl();

    try {
      FileReader fileReader = new FileReader(targetUrl);
      BufferedReader bufferedReader = new BufferedReader(fileReader);

      String line ;
      while((line = bufferedReader.readLine()) != null) {
        loadedLines.add(line);
      }

      bufferedReader.close();
    }
    catch(IOException ex) {
      errorMessage = "Error reading file '" + targetUrl + "'";
      errorMessage += "("+ ex.getMessage() +")";
      return null;
    }

    return loadedLines;
  }

  protected String getAddUrl() {
    if (addUrl.equals(addUrlC1)) {
      addUrl = addUrlC2;
    }
    else {
      addUrl = addUrlC1;
    }

    return addUrl;
  }

}
