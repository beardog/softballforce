package softballForce.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//

public class MainApp {

  private static final Logger logger = LoggerFactory.getLogger("softballForce.main");

  public static void main(String[] args) {
    logger.debug("application started"); //

    SoftballForceApp app = new SoftballForceApp();
    if (!app.doIt(args)) {
      logger.error(app.getErrorMessage());
    }

    logger.debug("application finished."); //
  }
}
