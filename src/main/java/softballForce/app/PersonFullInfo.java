package softballForce.app;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PersonFullInfo {
  public String firstName;
  public String middleName;
  public String surname;

  public String dateOfBirth;

  public String organization;
  public String jobTitle;
  public String checkResult;
  public String term;

  /**
   *   Fills basic name fields of the object.
   * @param fullName A string with full name (like "SURNAME Firstname Middlename")
   * @return true on success
   */
  public boolean readFullName(String fullName) {
    String pstr = "([\\p{L}']+\\s?\\-?\\s?[\\p{L}']+)\\s+([\\p{L}']+)\\s+([\\p{L}']+)";

    Pattern pattern = Pattern.compile(pstr);
    Matcher matcher = pattern.matcher(fullName);

    if (!matcher.find()) {
      return false;
    }

    surname=matcher.group(1);
    firstName=matcher.group(2);
    middleName = matcher.group(3);

    return true;
  }

  public void debugSelfPrint() {
    Logger logger = LoggerFactory.getLogger("softballForce.main");

    logger.debug("Info for {} :",
                 String.format("%s %s %s, born on %s",
                               firstName, middleName, surname, dateOfBirth));
    logger.debug("* From {}", organization);
    logger.debug("* Worked as {}", jobTitle);
    logger.debug("* Fired because of {}", checkResult);
    logger.debug("* For {}", term);

  }
}