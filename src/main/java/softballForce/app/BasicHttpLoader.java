package softballForce.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//
import java.util.List;


/**
 *
 */
public abstract class BasicHttpLoader {

  protected static final Logger logger = LoggerFactory.getLogger("softballForce.main");

  protected String targetUrl = "";
  protected String errorMessage = "";

  public BasicHttpLoader() {
    //nothing to do here
  }

  /*
  *
  */
  public String getErrorMessage() {
    return errorMessage;
  }

  public boolean setUrl(String url){
    targetUrl = url;
    return true;
  }

  protected abstract List<String> getRawContent() ;

  public List<PersonInfo> load() {
    List<String> rawLines = getRawContent();
    if (rawLines==null) {
      return null;
    }

    RegisterHtmlProcessor registerHtmlProcessor = new RegisterHtmlProcessor();
    List<PersonInfo> result = registerHtmlProcessor.process(rawLines) ;
    if (result ==null) {
      errorMessage=registerHtmlProcessor.getErrorMessage();
    }

    return result;
  }

}
