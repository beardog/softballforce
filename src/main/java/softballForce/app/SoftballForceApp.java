package softballForce.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//
import cottonfalcon.CottonFalcon;
//
import java.util.LinkedList;
import java.util.List;
//


/**
 * Main class of the SoftballForce app.
 */
public class SoftballForceApp {

  private static final Logger logger = LoggerFactory.getLogger("softballForce.main");

  static final String logLevelShCO = "l";
  static final String logLevelLCO = "loglevel";

  static final String showHelpShCO = "h";
  static final String showHelpLCO = "help";

  static final String useFakeLCO = "useFake";

  static final String urlLCO = "url";
  static final String detailedUrlLCO = "url2";
  static final String yamlFileNameLCO = "yaml";
  static final String newYamlFileNameLCO = "newYaml";

  String errorMessage = "";

  String urlToProcess = "";
  String detailedUrlToProcess = "";

  boolean useFakeData = false;

  String yamlFileName = "";
  String newYamlFileName = "";



  public String getErrorMessage() {
    return errorMessage;
  }

  protected void printHelpMessage() {
    System.out.println("Application accepts following options:");
    System.out.println("-h or --help              Show this message and exit");
    System.out.println("-l or --loglevel [level]  Change logging level. " +
            "Possible values are \"DEBUG\", \"INFO\", \"WARN\"" +
            "and \"ERROR\". Default is \"WARN\". ");
    System.out.println("--" + urlLCO
            + "    An url to download from (temporary fake "
            + "implementation expects filename here) ");
    System.out.println("--" + detailedUrlLCO
            + "    Another url to download detailed information on person "
            + "(temporary fake implementation expects folder name here) ");
    System.out.println("--" + useFakeLCO
            + "    Application will try to load data from files instead of "
            + "sending http requests");
    System.out.println("--" + yamlFileNameLCO
            + "    A file to load previously received data (using YAML format) ");
    System.out.println("--" + newYamlFileNameLCO
            + "    A file to store received data (using YAML format) ");
  }

  protected boolean processArgs(String[] args) {
    CottonFalcon cf = new CottonFalcon();
    cf.addOption(showHelpShCO, showHelpLCO, false);
    cf.addLongOption(urlLCO, true);
    cf.addLongOption(detailedUrlLCO, true);

    cf.addLongOption(useFakeLCO, false);

    cf.addOption(logLevelShCO, logLevelLCO, true);

    cf.addLongOption(yamlFileNameLCO, true);
    cf.addLongOption(newYamlFileNameLCO, true);


    boolean cfpr = cf.process(args);
    if (!cfpr) {
      errorMessage = cf.getErrorMessage();
      return false;
    }

    if (cf.gotShortOption(showHelpShCO)||cf.gotLongOption(showHelpLCO)) {
      logger.debug("need to show help message"); //
      printHelpMessage();
      return false;
    }

    //--
    if (cf.gotShortOption(logLevelShCO)||cf.gotLongOption(logLevelLCO)) {
      String desiredLogLevel = cf.getOptionParameter(logLevelShCO, logLevelLCO);
      if (desiredLogLevel.equals("DEBUG")) {
        ((ch.qos.logback.classic.Logger)logger).setLevel(ch.qos.logback.classic.Level.DEBUG);
      } else if (desiredLogLevel.equals("INFO")) {
        ((ch.qos.logback.classic.Logger)logger).setLevel(ch.qos.logback.classic.Level.INFO);
      } else if (desiredLogLevel.equals("WARN")) {
        ((ch.qos.logback.classic.Logger)logger).setLevel(ch.qos.logback.classic.Level.WARN);
      } else if (desiredLogLevel.equals("ERROR")) {
        ((ch.qos.logback.classic.Logger)logger).setLevel(ch.qos.logback.classic.Level.ERROR);
      }
      else {
        errorMessage = String.format("Unknown logging level \"%s\"", desiredLogLevel);
        return false ;
      }
    }

    if (cf.gotLongOption(urlLCO)) {
      urlToProcess = cf.getLongOptionParameter(urlLCO);
    }
    else {
      errorMessage = String.format("Please specify url to be processed." +
                                   "(\"--%s\" option)", urlLCO);
      return false;
    }

    if (cf.gotLongOption(detailedUrlLCO)) {
      detailedUrlToProcess = cf.getLongOptionParameter(detailedUrlLCO);
    }
    else {
      errorMessage = String.format("Please specify basic url for detailed " +
               "information (\"--%s\" option).", detailedUrlLCO);
      return false;
    }

    if (cf.gotLongOption(useFakeLCO)) {
      useFakeData = true;
      logger.info("Application will try to use test data.");
    }

    if (cf.gotLongOption(yamlFileNameLCO)) {
      yamlFileName = cf.getLongOptionParameter(yamlFileNameLCO);
    }
    else {
      logger.warn("Yaml filename was not specified (--yaml option).");
    }

    if (cf.gotLongOption(newYamlFileNameLCO)) {
      newYamlFileName = cf.getLongOptionParameter(newYamlFileNameLCO);
    }
    else {
      errorMessage = String.format("Please specify result yaml file name " +
              "(\"--%s\" option).", newYamlFileNameLCO);
      return false;
    }

    //finally
    return true;
  }

  protected List<PersonInfo> loadPersons() {
    // Load html content from main page (person names)
    BasicHttpLoader loader = useFakeData ? new FakeHttpLoader() : new WebHttpLoader();

    loader.setUrl(urlToProcess);

    List<PersonInfo> result = loader.load();
    if (result == null) {
      errorMessage = loader.getErrorMessage() ;
      //return null;
    }
    return result;
  }

  protected List<PersonFullInfo> loadFullInfo(List<PersonInfo> names) {
    List<PersonFullInfo> result = new LinkedList<>();

    BasicPostLoader loader;
    if (useFakeData) {
      loader = new FakePostLoader(detailedUrlToProcess);
    }
    else {
      loader = new WebPostLoader(detailedUrlToProcess);
    }

    for (int i=0; i<2; i++) {
      PersonInfo personInfo = names.get(i+25);//getting person at some number

      PersonFullInfo fullInfo = loader.load(personInfo);
      if (fullInfo==null) {
        logger.warn("Failed to get info on {} ({})", personInfo.surname,
                    loader.getErrorMessage());
      }
      else {
        result.add(fullInfo);
      }
    }

    return result;
  }

  PersonFullInfo loadOneFullInfo(PersonInfo personInfo) {
    PersonFullInfo result = null ;

    BasicPostLoader loader;
    if (useFakeData) {
      loader = new FakePostLoader(detailedUrlToProcess);
      logger.warn("Loading full fake info for {}", String.format("%s %s %s",
              personInfo.firstName, personInfo.middleName, personInfo.surname));
    }
    else {
      loader = new WebPostLoader(detailedUrlToProcess);
      logger.warn("Loading full real info for {}", String.format("%s %s %s",
              personInfo.firstName, personInfo.middleName, personInfo.surname));
    }

    result = loader.load(personInfo);
    if (result==null) {
      logger.warn("Failed to get info on {} ({})", personInfo.surname,
              loader.getErrorMessage());
    }

    return result;
  }

  /**
   *
   * @return List of items, or empty list if any error
   */
  List<PersonFullInfo> readYamlInfo() {
    List<PersonFullInfo> result = null;

    if (!yamlFileName.isEmpty()) {
      YamlInfoReader yamlInfoReader = new YamlInfoReader();
      result = yamlInfoReader.readFromFile(yamlFileName);
      if (result == null) {
        logger.warn("Failed to read old yaml file ({}), message: {}",
                yamlFileName, yamlInfoReader.getErrorMessage());
      }
    }

    if (result == null) {
      result = new LinkedList<>();
    }

    return result;
  }


  List<PersonFullInfo> prepareNewInfo(List<PersonInfo> personInfoList) {
    List<PersonFullInfo> result = null;

    //Load info from file (so called "old")
    List<PersonFullInfo> oldInfoList = readYamlInfo();

    result = new LinkedList<>();

    for(PersonInfo personInfo:personInfoList) {
      boolean found = false;

      logger.debug("Working with {}", String.format("%s %s %s",
              personInfo.firstName, personInfo.middleName, personInfo.surname));

      for(PersonFullInfo oldInfo:oldInfoList) {
        if (oldInfo.surname.equals(personInfo.surname) &&
            oldInfo.firstName.equals(personInfo.firstName) &&
            oldInfo.middleName.equals(personInfo.middleName) &&
            oldInfo.dateOfBirth.equals(personInfo.dateOfBirth) ) {
          found = true;
          result.add(oldInfo);
          break;
        }
      }

      if (!found) {
        PersonFullInfo pfinfo = loadOneFullInfo(personInfo) ;
        if (pfinfo!=null ) {
          result.add(pfinfo);
        }
        else {
          logger.warn("Failed to get full info for {}", String.format("%s %s %s",
                  personInfo.firstName, personInfo.middleName, personInfo.surname));
        }
      }
    }

    return result;
  }

  /** Just saves information to file.
   *  File to save is #newYamlFileName.
   * @param infos Information to save
   * @return
   */
  boolean saveInfoList(List<PersonFullInfo> infos) {
    boolean result = true;

    YamlInfoWriter yamlInfoWriter = new YamlInfoWriter();
    if (!yamlInfoWriter.saveToFile(infos, newYamlFileName)) {
      errorMessage = yamlInfoWriter.getErrorMessage();
      result = false;
    }

    return result;
  }

  /**
   * Performs all required actions.
   * @param args   command line arguments, as in "main".
   * @return true on success, false on some error.
   */
  public boolean doIt(String[] args) {

    // Process command line arguments
    if (!processArgs(args)) {
      //logger.error(errorMessage);
      return false;
    }

    List<PersonInfo> persons = loadPersons();
    if (persons==null) {
      return false;
    }
    logger.debug("Got {} names", persons.size());

    List<PersonFullInfo> newInfoList = prepareNewInfo(persons);
    if (newInfoList==null) {
      return false;
    }

    if (!saveInfoList(newInfoList)) {
      return false;
    }

    return true;
  }
}
